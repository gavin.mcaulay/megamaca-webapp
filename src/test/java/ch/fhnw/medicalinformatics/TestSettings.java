package ch.fhnw.medicalinformatics;

public class TestSettings {

	public static final boolean EXECUTE_WEBTESTS = false;
	
	public final static String URL = "http://localhost:8090/mscmi_geneinfo_webapp/";
	
}
