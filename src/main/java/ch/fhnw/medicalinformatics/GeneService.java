package ch.fhnw.medicalinformatics;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.event.NamedEvent;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import okhttp3.*;
import javax.swing.SwingUtilities;

@NamedEvent
@ApplicationScoped
@ManagedBean(name = "geneService")
public class GeneService {

	private List<String> searchOption;
	private String searchTerm;
	private List<Gene> data = new ArrayList<>();
	private String hostname = "http://localhost:8080";
	
	private final OkHttpClient httpClient;
	
	public GeneService() {
		httpClient = new OkHttpClient.Builder()
			      .connectTimeout(180, TimeUnit.SECONDS)
			      .readTimeout(180, TimeUnit.SECONDS)
			      .writeTimeout(180, TimeUnit.SECONDS)
			      .build();
	}
	
	public List<String> getSearchOptions() {
		List<String> result = new ArrayList<String>();
		result.add("ID");
		result.add("Symbol");
		result.add("Description");
		return result;
	}	
	
	public List<String> getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(List<String> searchOption) {
		this.searchOption = searchOption;
	}
	
	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public void retrieveData() {
		System.out.println("retrieve data");
		data.clear();
		// retrieve data from the service
		Request request1 = null;
		Request request2 = null;
		Request request3 = null;
		String serviceCall1 = "";
		String serviceCall2 = "";
		String serviceCall3 = "";
		if (searchOption.contains("ID") && searchOption.contains("Symbol") && searchOption.contains("Description")) {
			serviceCall1 = "/geneservice/byid?id=" + searchTerm;
			serviceCall2 = "/geneservice/bysymbol?symbol=" + searchTerm;
			serviceCall3 = "/geneservice/bydescription?description=" + searchTerm;
		} else if (searchOption.contains("ID") && searchOption.contains("Symbol")) {
			serviceCall1 = "/geneservice/byid?id=" + searchTerm;
			serviceCall2 = "/geneservice/bysymbol?symbol=" + searchTerm;
		} else if (searchOption.contains("ID") && searchOption.contains("Description")) {
			serviceCall1 = "/geneservice/byid?id=" + searchTerm;
			serviceCall3 = "/geneservice/bydescription?description=" + searchTerm;
		} else if (searchOption.contains("Symbol") && searchOption.contains("Description")) {
			serviceCall2 = "/geneservice/bysymbol?symbol=" + searchTerm;
			serviceCall3 = "/geneservice/bydescription?description=" + searchTerm;
		} else if (searchOption.contains("ID")) {
			serviceCall1 = "/geneservice/byid?id=" + searchTerm;
		} else if (searchOption.contains("Symbol")) {
			serviceCall2 = "/geneservice/bysymbol?symbol=" + searchTerm;
		} else if (searchOption.contains("Description")) {
			serviceCall3 = "/geneservice/bydescription?description=" + searchTerm;
		} else {
			System.out.println("invalid search");
			// TODO: Exception Handling
		}

		System.out.println("URL: " + hostname + serviceCall1);
		System.out.println("URL: " + hostname + serviceCall2);
		System.out.println("URL: " + hostname + serviceCall3);

		request1 = new Request.Builder().url(hostname + serviceCall1).build();
		request2 = new Request.Builder().url(hostname + serviceCall2).build();
		request3 = new Request.Builder().url(hostname + serviceCall3).build();

		httpClient.newCall(request1).enqueue(new Callback() {

			@Override
			public void onFailure(Call call, IOException e) {
				e.printStackTrace();
			}

			@Override
			public void onResponse(Call call, Response response1) throws IOException {
				Gson g = new Gson();
				Type resultType = null;
				resultType = new TypeToken<Gene>() {}.getType();
				Gene sg1 = g.fromJson(response1.body().string(), resultType);
				data.add(sg1);
			}
		});

		httpClient.newCall(request2).enqueue(new Callback() {

			@Override
			public void onFailure(Call call, IOException e) {
				e.printStackTrace();
			}

			@Override
			public void onResponse(Call call, Response response2) throws IOException {
				Gson g = new Gson();
				Type resultType = null;
				resultType = new TypeToken<List<Gene>>() {}.getType();
				data.addAll(g.fromJson(response2.body().string(), resultType));
			}
		});

		httpClient.newCall(request3).enqueue(new Callback() {

			@Override
			public void onFailure(Call call, IOException e) {
				e.printStackTrace();
			}

			@Override
			public void onResponse(Call call, Response response3) throws IOException {
				Gson g = new Gson();
				Type resultType = null;
				resultType = new TypeToken<List<Gene>>() {}.getType();
				data.addAll(g.fromJson(response3.body().string(), resultType));
			}
		});
	}
	
	public List<Gene> getGenes() {
		return data;
	}
}
